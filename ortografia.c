//Nome: Marcus Augusto Ferreira Dudeque - GRR: 20171616

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dicionario.h"
#define FIRST_LETTER 0
#define WORD_SIZE 50
#define DICT_SIZE 275502

int is_letter(unsigned char aux) //checks if it is a letter
{
  if (((aux >= 65) && (aux <= 90)) || ((aux >= 97) && (aux <= 122))||
      ((aux >= 192) && (aux <= 207)) || ((aux >= 210) && (aux <= 214)) ||
      ((aux >= 217) && (aux <= 221)) || ((aux >= 224) && (aux <= 229)) ||
      ((aux >= 231) && (aux <= 239)) || ((aux >= 241) && (aux <= 246)) ||
      ((aux >= 249) && (aux <= 253)))
    return (1);
  else
    return (0);
}

int is_upper(unsigned char aux) //checks if it is upper case leter
{
  if (((aux >= 65) && (aux <= 90)) || ((aux >= 192) && (aux <= 214)) ||
      ((aux >= 217) && (aux <= 221)))
    return (1);
  else
    return (0);
}

void clear_word(unsigned char word[WORD_SIZE]) //fills with '\0'
{
  int i;
  for (i = 0; i < WORD_SIZE; ++i)
  {
    word[i] = '\0';
  }
}

void turns_lower(unsigned char word_aux[WORD_SIZE])
{
  int i = 0;
  while (word_aux[i] != '\0')
  {
    if (is_upper(word_aux[i]))
      word_aux[i] += 32;
    ++i;
  }
}

int main()
{
  store_dict(); //calls function that stores dictionary in memory  

  unsigned char word[WORD_SIZE];
  unsigned char word_aux[WORD_SIZE];
  unsigned char aux;
  int i = 0;
  int found = 0;

  clear_word(word); //fills word with '\0'


  aux = getc(stdin); 
  while ((aux != EOF) && (aux != 255) && (aux != 0xFF))
  {
    while (!(is_letter(aux)) && (aux != 255))
    {
      printf("%c", aux); //prints non-letters characters
      aux = getc(stdin);
    }
    while (is_letter(aux))
    {
      word[i] = aux; //stores word
      aux = getc(stdin); 
      ++i;
    }
    i = 0;
    found = search_word(word, FIRST_LETTER, DICT_SIZE); //search for word as it is
    if (!found && is_upper(word[0])) //checks if it is upper case
    {
      strcpy((char *)word_aux, (char *)word);      
      turns_lower(word_aux);
      found = search_word(word_aux, FIRST_LETTER, DICT_SIZE);
    }
    if (found)
    {
      printf("%s", word); //prints correct words
    }
    else
    {
      printf("[%s]", word); //prints incorrect words in between []
    }
    clear_word(word);
  }  
}
