//Nome: Marcus Augusto Ferreira Dudeque - GRR: 20171616

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define FIRST_LETTER 0
#define WORD_SIZE 50

unsigned char **matrix;

int countline(FILE *dict) //counts number of words
{
  long int n_lines = 0;
  char                *aux1 = NULL;
  unsigned long int    aux2 = 0;
  while (getline (&aux1, &aux2, dict) != -1) 
    ++n_lines;
  return(n_lines);
}

void store_dict(void) //stores dictionary
{
  int n_words = 0;
  int i, j = 0;
  char aux;
  FILE *dict = fopen("/usr/share/dict/brazilian", "r"); //opens dictionary file  
  if (dict == NULL) //checks validity of file
  {
    perror("Unable to open file.\n");
    exit(1);
  }
  else
  {    
    n_words = countline(dict); //calls countline to get number of words
    rewind(dict); //gets pointer back to begin of file
    matrix = malloc(n_words * sizeof(*matrix)); //malloc for lines
    for (i = 0; i < (n_words); ++i)
      matrix[i] = malloc (WORD_SIZE * (sizeof(char))); //malloc for columns
    i = 0;
    aux = fgetc(dict); //aux gets 1st char from dict
    while (aux != EOF) //stores dictionary at memory
    {
      while (aux != '\n')
      { 
        matrix[i][j] = aux;   
        aux = fgetc(dict);
        ++j;     
      }
      aux = fgetc(dict); //gets char again so it goes different from \n
      j = 0;
      ++i;
    }
  }
  fclose(dict);
}

//binary search for word
int search_word(unsigned char *word, int begin, int end)
{
  int mid;
  unsigned char middle_word[WORD_SIZE];
  if (begin > end)
    return 0;

  mid = floor((begin + end) / 2);
  strcpy((char *)middle_word, (char *)matrix[mid]);

  if ((strcmp((char *)word, (char *)middle_word)) == 0)
    return (1);
  if ((strcmp((char *)word, (char *)middle_word)) < 0)
    return search_word(word, begin, (mid - 1));

  return search_word(word, (mid + 1), end);
}